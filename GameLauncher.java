import java.util.Scanner;
public class Application{
	public static void main(String [] args){
		Scanner reader = new Scanner(System.in);
		System.out.println("press 1 to play Hangman or press 2 to play Wordle");
		int answer = reader.nextInt();
		reader.nextLine();
		if (answer == 1){
			System.out.println("give a word");
			String word = reader.nextLine();
			System.out.print("\033[h\033[2J");
			System.out.flush();
			Hangman.runGame(word);
		}
		else if (answer == 2){
			String word = Wordle.generateWord();
			Wordle.runGame(word);
		}
	}
}