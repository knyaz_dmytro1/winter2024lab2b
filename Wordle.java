import java.util.Scanner;
import java.util.Random;
public class Wordle{
    public static String generateWord(){
        String[] listOfWords = new String[]{"crazy", "Sharp", "Shelf", "Shine", "Eight", "mixed", "bacon", "naked", "pound", "found", "cloud", "ghost", "faith", "fancy", "right", "night", "about", "table", "faces", "hacks", "sport", "alive", "death"};
        Random rand = new Random();
        int word = rand.nextInt(22);
        return listOfWords[5];
    }
    public static boolean letterInWord(String word, char letter){
        for (int i = 0; i<word.length() ; i++){
            if (word.toUpperCase().charAt(i) == letter){
                return true;
            }
        }
        return false;
    }
    public static boolean letterInSlot(String word, char letter, int position){
        if (word.toUpperCase().charAt(position) == letter){
            return true;
        }
        return false;
    }
    public static String[] guessWord(String answer, String guess){
        String[] colors = new String[]{"white", "white", "white", "white", "white"};
        for (int i = 0 ; i < 5 ; i++){
            if (letterInWord(answer, guess.charAt(i))){
                colors[i] = "yellow"; 
            }    
            if (letterInSlot(answer, guess.charAt(i), i)){
                colors[i] = "green";
            } 
        }
        return colors;
    } 
    public static void presentResults(String word, String[]colors){
        for (int i = 0 ; i<5 ; i++){
            if (colors[i].equals("green")){
                colors[i] = "\u001B[32m";
            }
            if (colors[i].equals("yellow")){
                colors[i] = "\u001B[33m";
            }
            if (colors[i].equals("white")){
                colors[i] = "\u001B[0m";
            }
        }
        System.out.println(colors[0] + word.substring(0, 1) + colors[1] + word.substring(1, 2) + colors[2] + word.substring(2, 3) + colors[3] + word.substring(3, 4) + colors[4] + word.substring(4, 5) + "\u001B[0m");
    }
    public static String readGuess(){
        Scanner reader = new Scanner(System.in);
        System.out.println("Guess a 5 letter word");
        String guess = reader.nextLine();
        while(guess.length() != 5){
            System.out.println("I SAID A 5 LETTER WORD!");
            guess = reader.nextLine();
        }
        return guess.toUpperCase();
    }
    public static void runGame(String word){
        int attempts = 0;
        String [] colors = new String[]{"white", "white", "white", "white", "white"}; 
        boolean isGuessCorrect = false;
        for ( attempts = 0 ; attempts < 6 && !isGuessCorrect ; attempts++){
            String guess = readGuess();
            colors = guessWord(word, guess);
            if (colors[0] == "green" && colors[1] == "green" && colors[2] == "green" && colors[3] == "green" && colors[4] == "green"){
                isGuessCorrect = true;
            }
            presentResults(guess, colors);
            
        }
        if (attempts == 6 && isGuessCorrect == false){
            System.out.println("Try again, LOSER!!!");
        }
        if (isGuessCorrect == true){
            System.out.println("You Won!!!");
        }
        

    }
}